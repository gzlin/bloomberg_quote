from lxml import html
import requests
import sqlite3
import time

start = time.clock()

conn = sqlite3.connect('russ2k.sqlite')
cur = conn.cursor()

bad_symbols = []
under_seven = []

data = cur.execute("Select * FROM Russ2k")
rows = data.fetchall()
for row in rows:
    id = row[0]
    print id
    ticker = row[1]
    print ticker
    url = "https://finance.yahoo.com/q/hp?s="+ ticker + "+Historical+Prices"
    try:
        page = requests.get(url)
        print page
    except:
        print "uh oh" 
    tree = html.fromstring(page.content)

    info = tree.xpath('//td[@class = "yfnc_tabledata1"]/text()')
    try:
        OY = info[1]
        if OY < 7.50:
            under_seven.append(ticker)
            cur.execute("DELETE FROM Russ2k WHERE symbol = ?", (ticker, ))
            continue
    except:
        print ticker, "is fucked"
        bad_symbols.append(ticker)
        cur.execute("DELETE FROM Russ2k WHERE symbol = ?", (ticker, ))
        continue
    HY = info[2]
    LY = info[3]
    CY = info[4]

    O2 = info[8]
    H2 = info[9]
    L2 = info[10]
    C2 = info[11]

    O3 = info[15]
    H3 = info[16]
    L3 = info[17]
    C3 = info[18]
    cur.execute("""INSERT OR REPLACE INTO Historical 
        (his_id, oy, hy, ly, cy, o2, h2, l2, c2, o3, h3, l3, c3)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""",
        (id, OY, HY, LY, CY, O2, H2, L2, C2, O3, H3, L3, C3))
    conn.commit()

end = time.clock()
print "Total time = ", end - start `